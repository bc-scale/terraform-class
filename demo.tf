/* -------------------------------------------------------------------------------
Creating a namespace - Basics
------------------------------------------------------------------------------ */
variable "my_namespace" {
    type = string
    default = "xxxx"
}

variable "my_labels" {
    type = map(string)
    default = {
        builder = "terraform"
        user = "xxxx"
    }
}

/* -------------------------------------------------------------------------------
Typically you'll start with a namespace for your application
------------------------------------------------------------------------------ */
resource "kubernetes_namespace_v1" "my_namespace" {
    metadata {
        name = var.my_namespace
        labels = merge(var.my_labels,{})
    }
}

/* -------------------------------------------------------------------------------
Deploy a kubernetes pod into your namespace using either the variable or
a the new objects metadata reference.
------------------------------------------------------------------------------ */
resource "kubernetes_pod" "my_pod" {
  metadata {
    name      = "dnsutils"
    namespace = var.my_namespace
    #intentionally incorrect for testing
    labels = merge(var.mylabels,{})
    #alternatively: namespace = kubernetes_namespace_v1.mine.metadata.0.name
  }

  spec {
    container {
      image = "k8s.gcr.io/e2e-test-images/jessie-dnsutils:1.3"
      name  = "dnsutils"
      command = [
        "sleep", "3600"
      ]
      image_pull_policy = "IfNotPresent"

    }
    restart_policy = "Always"
  }
}









